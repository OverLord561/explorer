﻿using Explorer.Models;
using Explorer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Explorer.Extensions
{
    public static class ModelExtensions
    {
        public static FolderViewModel ToViewModel(this Folders model)
        {
            return new FolderViewModel
            {
                Id = model.Id,
                ParentId = model.ParentId.Value,
                Name = model.Name,
                CreationDate = model.CreationDate.Value,
                NestedFolders = model.Folders1.Select(x => x.ToNestedViewModel()).ToList()
            };
        }

        private static NestedFolderViewModel ToNestedViewModel(this Folders model)
        {
            return new NestedFolderViewModel
            {
                Id = model.Id,
                ParentId = model.ParentId.Value,
                Name = model.Name,
                CreationDate = model.CreationDate.Value,
            };
        }

    }
}