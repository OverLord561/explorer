﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Explorer.ViewModels
{
    /*
         At first was idea to use current class for nested folders like:  public IEnumerable<FolderViewModel> NestedFolders { get; set; }
         but AutoMapper does not support recursive projection for current case in EF6 except EF Core (c)(Stack Overflow).
         To solve this problem simplified NestedFolderViewModel was added.
         So AM now works fine and Extension too.
             */
    public class FolderViewModel
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }        
        public IEnumerable<NestedFolderViewModel> NestedFolders { get; set; }
    }
}