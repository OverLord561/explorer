﻿using Explorer.Repositories;
using Explorer.Services.Interfaces;
using Explorer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Explorer.Controllers
{
    public class ExplorerController : AsyncController
    {

        private IFoldersService _foldersService;

        public ExplorerController(IFoldersService foldersService)
        {
            this._foldersService = foldersService;
        }
        // GET: Folders
        public async Task<ActionResult> Index(string path, int? id)
        {
            FolderViewModel rootFolder = null;
            if (path == null)
            {
                rootFolder = await _foldersService.GetRootFolder();
            }
            else
            {
                rootFolder = await _foldersService.GetRootFolderById(id.Value);
            }
            if (rootFolder == null) return new HttpStatusCodeResult(HttpStatusCode.NotFound, "Folder doesn't exist");
            
            return View(rootFolder);
        }
    }
}