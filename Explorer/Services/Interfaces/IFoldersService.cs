﻿using Explorer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Explorer.Services.Interfaces
{
    public interface IFoldersService
    {
        Task<FolderViewModel> GetRootFolder();
        Task<FolderViewModel> GetRootFolderById(int id);

    }

}
