﻿using Explorer.Services.Interfaces;
using Explorer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Explorer.Extensions;
using Explorer.Repositories;
using Explorer.Models;

namespace Explorer.Services
{

    public class FoldersService : IFoldersService
    {
        private IFoldersRepository _foldersRepository;

        public FoldersService(IFoldersRepository foldersRepository)
        {
            _foldersRepository = foldersRepository ?? throw new ArgumentNullException(nameof(foldersRepository));
        }
        public async Task<FolderViewModel> GetRootFolder()
        {
            return await _foldersRepository.SingleOrDefaultAsync<FolderViewModel>(x => x.ParentId == null);
        }

        public async Task<FolderViewModel> GetRootFolderById(int id)
        {
            return await _foldersRepository.SingleOrDefaultAsync<FolderViewModel>(x => x.Id == id);
        }
    }

}
