﻿using AutoMapper;
using Explorer.Models;
using Explorer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Explorer.Mapping
{
    public class NestedFoldersMappingProfile : Profile
    {
        public NestedFoldersMappingProfile()
        {
            CreateMap<Folders, NestedFolderViewModel>()
                .ForMember(dest => dest.Id,
                           opts => opts.MapFrom(src => src.Id))
                            .ForMember(dest => dest.Name,
                           opts => opts.MapFrom(src => src.Name))
                            .ForMember(dest => dest.ParentId,
                           opts => opts.MapFrom(src => src.ParentId))
                            .ForMember(dest => dest.CreationDate,
                           opts => opts.MapFrom(src => src.CreationDate));
        }
    }
}