﻿using Explorer.Models;
using Explorer.ViewModels;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Explorer.Extensions;

namespace Explorer.Mapping
{

    public class FoldersMappingProfile : Profile
    {
        public FoldersMappingProfile()
        {
            CreateMap<Folders, FolderViewModel>()
                .ForMember(dest => dest.Id,
                           opts => opts.MapFrom(src => src.Id))
                            .ForMember(dest => dest.Name,
                           opts => opts.MapFrom(src => src.Name))
                            .ForMember(dest => dest.ParentId,
                           opts => opts.MapFrom(src => src.ParentId))
                            .ForMember(dest => dest.CreationDate,
                           opts => opts.MapFrom(src => src.CreationDate))
                            .ForMember(dest => dest.NestedFolders,
                           opts => opts.MapFrom(src => src.Folders1.Select(y => new NestedFolderViewModel()
                           {
                               Id = y.Id,
                               ParentId = y.ParentId.Value,
                               Name = y.Name,
                               CreationDate = y.CreationDate.Value
                           }))).MaxDepth(2);


        }
    }

}