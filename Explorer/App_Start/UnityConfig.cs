using Explorer.Repositories;
using Explorer.Repositories.EntityFramework;
using Explorer.Services;
using Explorer.Services.Interfaces;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace Explorer
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IFoldersRepository, FoldersRepository>();
            container.RegisterType<IFoldersService, FoldersService>();
            
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}