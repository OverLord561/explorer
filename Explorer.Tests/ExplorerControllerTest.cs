﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using Explorer.Controllers;
using Explorer.Services.Interfaces;
using Explorer.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Explorer.Tests
{
    [TestClass]
    public class ExplorerControllerTest
    {
        private Mock<IFoldersService> _foldersServiceMock;

        [TestMethod]
        public void Home_Controller_Returns_Error_View()
        {
            var hc = new HomeController();

            var result = hc.Error() as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task Explorer_Controller_Returns_Index_View()
        {
            var fc = GetExplorerController();
            SetupGetRootFolder(new FolderViewModel());

            var result = await fc.Index(null, null) as ViewResult;

            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task Index_Should_Return_400_Status_Code()
        {
            var fc = GetExplorerController();
            SetupGetRootFolder(null);

            var result = await fc.Index(null, null) as HttpStatusCodeResult;

            Assert.AreEqual((int)HttpStatusCode.NotFound,result.StatusCode);
        }

        
        private void SetupGetRootFolder(FolderViewModel model)
        {
            _foldersServiceMock.Setup(x => x.GetRootFolder())
                .Returns(Task.FromResult(model));
        }

        private ExplorerController GetExplorerController()
        {
            _foldersServiceMock = new Mock<IFoldersService>();


            ExplorerController ec = new ExplorerController(
                _foldersServiceMock.Object);


            return ec;
        }
    }
    
}
